export const generateProductCatalog = (product) => `<div class="roundedOne"><label class="container"><span class="checkmark"></span>
<input class="c1" type="checkbox" name="checkbox" onchange="checkboxes()"><div class="catalog__product">
<div class="catalog__photo">
<div class="product-image"><img src="${product.image}"/></div>
<div class="add-inventory"> <button class="inventory">Add To Inventory</button></div>
</div>
<div class="catalog__title">
    <p>${product.title}</p>
</div>
<!-- <hr> -->
<div class="catalog__price">
    <div class="RRP">
        <p>RRP: ${(Math.round((product.price / 30) * 100)) + product.price}</p>
    </div>
    <div class="Profit">
        <p>Profit: 32% / $${Math.round((product.price * 30) / 100) }</p>
    </div>
    <div class="Cost">
        <p>Cost: ${product.price}</p>
    </div>
</div>
</div>


</label>
</div>
`;

export const selectedProductsNum = (num) => `              
<p>
  selected ${num} out of 274,670 products
</p>
`;

