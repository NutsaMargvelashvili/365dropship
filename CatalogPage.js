import { products } from './API.js';
import { generateProductCatalog, selectedProductsNum} from './Product.js' 

const catalog = document.getElementsByClassName('catalog')[0];
const selectedProducts = document.getElementById('select-number')


const generateProductsHTML = productList => {
    let productHTML = '';
    let maxProductPrice = 0;
    for(const product of productList) {
        if(product.price > maxProductPrice){
            maxProductPrice = product.price;
        }
        productHTML += generateProductCatalog(product);
    }
    document.getElementById('minPriceSlide').max = maxProductPrice;
    document.getElementById('maxPriceSlide').max = maxProductPrice;
    return productHTML;
}
products().then(result => {
    catalog.innerHTML = generateProductsHTML(result);
}).finally(function() { 
    document.getElementsByClassName('loading')[0].style.display = 'none';
});
console.log(document.getElementsByClassName('sideBar')[0].style);
//sort

document.querySelector('[name=sort]').addEventListener('change', event => {
    products(event.target.value).then((result) => {
        catalog.innerHTML = generateProductsHTML(result);
    });
});

//search

document.getElementById('search').addEventListener('click', event => {
    products().then(result => {
        let word =  event.target.value.charAt(0).toUpperCase() + event.target.value.slice(1).toLowerCase();
        let filtered = result.filter(product => product.title.includes(word));
        catalog.innerHTML = generateProductsHTML(filtered);
    });
});
 window.checkboxes = function() {
        let num = document.querySelectorAll('input[type="checkbox"]:checked').length;
        (num > 0)
           ? document.getElementsByTagName('button')[2].style.display = "block"
           : document.getElementsByTagName('button')[2].style.display = "none"; 
        selectedProducts.innerHTML = selectedProductsNum(num);
    };

   
//select all

window.selects = function(){  
        let items = document.getElementsByName('checkbox');  
        for(const item of items){  
            if(item.type == 'checkbox') item.checked = true;  
        }  
        document.getElementsByTagName('button')[2].style.display = "block";
        checkboxes();
    }  

//deselect all

window.deSelect = function(){  
    let items = document.getElementsByName('checkbox');  
    for(const item of items){  
        if(item.type == 'checkbox') item.checked = false;  
    }  
    document.getElementsByTagName('button')[2].style.display = "none";
    checkboxes();
}  

//price range

window.minRange = function(minVal){  
        document.getElementById('minRange').value = minVal;
        
}  

window.maxRange = function(maxVal){  
        document.getElementById('maxRange').value = maxVal;
       
}  


window.minPriceRange = function(minVal){  
    document.getElementById('minPrice').value = minVal;
    searchMinRange(minVal);
}  

window.maxPriceRange = function(maxVal){  
    document.getElementById('maxPrice').value = maxVal;
    searchMaxRange(maxVal);
}  


const searchMinRange = minRange => {
    products().then(result => {
    
        let filtered = result.filter(product => product.price >= minRange);
        catalog.innerHTML = generateProductsHTML(filtered);
    });
}

const searchMaxRange = maxRange => {
    products().then(result => {
    
        let filtered = result.filter(product => product.price <= maxRange);
        catalog.innerHTML = generateProductsHTML(filtered);
    });
}

console.log(document.getElementById('myRange').max);