import { API } from './Config.js';

const call = async (url) => {
  const request = await fetch(API + url);
  const result = await request.json();
  return result;
};

export const products = async (sort = null) =>{

return await call((sort == null) ? `/products` :`/products?sort=${sort}`);
}



export const categories = async () => await call('/products/categories');


